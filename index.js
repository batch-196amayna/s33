//express package was imported as express
const express = require("express");

//invoke express package to create a server/api and saved it in variable which we can refer to later to create routes
const app = express();

//express.json() is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from the request
//app.use() is a method used to run another function or method for our expressjs api.
//it is used to run middlewares (functions that add features to our application)
app.use(express.json())

//variable for port assignment
const port = 4000;

//mock Collection for courses
let courses = [
	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn ReactJS",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}
];

let users = [
	{
		email:"marybell_knight",
		password: "merrymarybell"
	},
	{
		email:"janedoePriest",
		password: "jobPriest100"
	},
	{
		email:"kinTofu",
		password: "dubuTofu"
	}
];


//used the listen() method of express to assign a port server and send a message
//creating a route in Express:
//access express to have access to its route methods
/*
	app.method('/endpoint',(request,response)=>{
	*send() is a method similar to end() that its sends the data/message and ends the response
	*it also automatically creates and adds the headers
	response.send()
	})
*/
// app.get('/',(req,res)=>{
// 	res.send("Hello from our first ExpressJS route!");
// })

app.get('/',(req,res)=>{
	res.send("Hello again");
})

app.post('/',(req,res)=>{
	res.send("Hello from our first ExpressJS Post method route")
})

app.put('/',(req,res)=>{
	res.send("Hello from a put method route!")
})

app.delete('/',(req,res)=>{
	res.send("Hello from delete method route!")
})

app.get('/courses',(req,res)=>{
	res.send(courses);
})

//create a route to be able to add a new course from a request
app.post('/courses',(req,res)=>{

	//with express.json() the data stream has been captured, the data input has been parse into a JS object

	//Note: Every time you need to access or use the request body

	courses.push(req.body);
	//console.log(courses);
	res.send(courses);
})

//Activity-----------------------------------

app.get('/users',(req,res)=>{
	res.send(users);
})


app.post('/users',(req,res)=>{
	users.push(req.body);
	res.send(users);
})

app.delete('/users',(req,res)=>{
	users.pop(req.body);
	res.send("User has been deleted");
})
//Activity-----------------------------------







app.listen(port,() => console.log(`Express API running at post 4000`))


